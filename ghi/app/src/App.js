import Nav from './Nav';
import React from 'react';

function App(props) {
  console.log(props)
  if (!props.attendees || props.attendees.length === 0) {
    return <div>No attendees found.</div>;
  }

  return (
    <React.Fragment>
      <Nav />
      <div className="container">
        <table className="table table-dark table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Conference</th>
            </tr>
          </thead>
          <tbody>
            {props.attendees.map((attendee) => (
              <tr key={attendee.id}>
                <td>{attendee.name}</td>
                <td>{attendee.conference}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </React.Fragment>
  );
}

export default App;